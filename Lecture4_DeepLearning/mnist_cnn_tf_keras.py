#!/usr/bin/env python
# coding: utf-8

# # Convolutional neural network (CNN) for digit classification (MNIST)
# 
# In this example, we use the MNIST dataset of handwritten digits and apply a convolutional neural network (CNN) to classify the digits.
# 
# As usual, we first import some modules.

# In[1]:


import numpy as np
from tensorflow import keras
from tensorflow.keras import layers


# And we load the dataset.

# In[2]:


# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()


# Let us have a closer look at the dataset, first by looking at the `shape` of the arrays, and then by plotting the images.

# In[3]:


print(x_train[1].shape)

import matplotlib.pyplot as plt
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')

num_row = 2
num_col = 5
# Plot images
fig, axes = plt.subplots(num_row, num_col, figsize=(1.5*num_col,2*num_row))
for i in range(num_row * num_col):
    ax = axes[i//num_col, i%num_col]
    ax.imshow(x_train[i], cmap='gray')
    ax.set_title('Label: {}'.format(y_train[i]))
plt.tight_layout()
plt.show()


# Let us preprocess the data a little bit.

# In[4]:


# Scale the "grayscale" between 0 and 1
x_train = x_train.astype("float32") / 255
x_test = x_test.astype("float32") / 255

# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)

print("y_train shape:", y_train.shape)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")

print(y_train[0:10])

# Model / data parameters
num_classes = 10

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

print(y_train[0:10])


# We can now define our model:
# - taking images of 28x28 with a "depth" from 0 to 1
# - using a convolutional layer of 32 nodes with a 3x3 convolution and a ReLU activation function
# - using a pooling layer of 2x2 (max)
# - using another conv layer of 64 nodes (same settings) with a 2x2 (max) pooling
# - we then flatten the output to achieve the classification
# - we use a 50% dropout rate (as we have 1600 nodes going into 10)
# - we finally have our output layer of 10 classes and use a softmax activation (to ensure the sum of probabilities is 1)

# In[5]:


input_shape = (28, 28, 1)

model = keras.Sequential(
    [
        keras.Input(shape=input_shape),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Flatten(),
        layers.Dropout(0.5),
        layers.Dense(num_classes, activation="softmax"),
    ]
)

# Shows a nice summary of the model
model.summary()


# Now let's train our model

# In[6]:


batch_size = 128
epochs = 15

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)


# Finally, we evaluate the model.

# In[7]:


score = model.evaluate(x_test, y_test, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])

