#!/usr/bin/env python
# coding: utf-8

# # MNIST dataset
# 
# The MNIST database (Modified National Institute of Standards and Technology database) is a large database of handwritten digits that is commonly used for training various image processing systems.

# In[1]:


import tensorflow as tf
mnist = tf.keras.datasets.mnist

# Defining training and testing smaples
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Normalising the dataset
x_train, x_test = x_train / 255.0, x_test / 255.0


# As usual, let's have a quick look at our dataset.

# In[2]:


import matplotlib.pyplot as plt
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')

num_row = 2
num_col = 5
# Plot images
fig, axes = plt.subplots(num_row, num_col, figsize=(1.5*num_col,2*num_row))
for i in range(num_row * num_col):
    ax = axes[i//num_col, i%num_col]
    ax.imshow(x_train[i], cmap='gray')
    ax.set_title('Label: {}'.format(y_train[i]))
plt.tight_layout()
plt.show()

print(x_train[1].shape)


# We now define our neural network: 
# - A sequence of layers which take as input 28x28 images which is flattened to a 1-dimensional array of size 784.
# - A dense intermediate layer with a ReLU activation
# - A dropout rate of 0.2
# - Finally, a dense output layer of size 10 (for each digit)

# In[3]:


model1 = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])


# We test the prediction of our (untrained) model on the first element of our training sample.

# In[4]:


predictions = model1(x_train[:1]).numpy()
print( predictions )


# The `tf.nn.softmax` function converts these logits to "probabilities" for each class.

# In[5]:


print( tf.nn.softmax(predictions).numpy() )


# We define our *loss* function and use it on our (untrained) model. This untrained model gives probabilities close to random (1/10 for each class), so the initial loss should be close to -tf.log(1/10) ~= 2.3.

# In[6]:


loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
print (loss_fn(y_train[:1], predictions).numpy())


# Let us now compile, using the ADAM optimiser and an accuracy metric, and train (fit) our model for 5 epochs.

# In[7]:


model1.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])

model1.fit(x_train, y_train, epochs=5)


# We can now evaluate the accuracy of the model for our testing dataset.
# 
# The image classifier is now trained to ~98% accuracy on this dataset.
# 
# Try increasing the number of epochs and see if it has any effect.

# In[8]:


model1.evaluate(x_test,  y_test, verbose=2)


# One can obtain a *probability model* by wrapping the model around a softmax function.

# In[9]:


probability_model = tf.keras.Sequential([
  model1,
  tf.keras.layers.Softmax()
])

for i in range(0,5):
  import numpy as np
  import matplotlib.pyplot as plt
  get_ipython().run_line_magic('matplotlib', 'inline')
  plt.imshow(x_test[i], cmap='gray')
  plt.show()
  mod_output = probability_model(x_test[i:i+1])
  print("Predicted number is", np.argmax(mod_output))


# This last snippet allows comparing the prediction with the actual value

# In[10]:


for i, p in enumerate(model1.predict(x_test)):
  print (y_test[i], np.argmax(p), y_test[i] == np.argmax(p))
  if i > 20: break


# 
# We can also train, evaluate and fit a new model `model2` with different settings for comparison.
# 
# Have fun playing with the parameters.
# 

# In[11]:


model2 = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(150, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])

model2.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])

model2.fit(x_train, y_train, epochs=5)

model2.evaluate(x_test,  y_test, verbose=2)

