#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from ROOT import TMVA
from ROOT import TFile
from ROOT import TCut
from ROOT import gSystem, gROOT, gApplication

from keras import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.regularizers import l2
from tensorflow.keras.optimizers import SGD

# Setup TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()


# In[ ]:


# output file
outNameLabel = "tmvaTraining"
outfileName = outNameLabel+".root"
outputFile = TFile.Open(outfileName, "RECREATE" );

# load the simulated data sets
inputSignal = TFile.Open("../Datasets/mc_345060.ggH125_ZZ4lep.4lep.root")
inputBG1 = TFile.Open("../Datasets/mc_363490.llll.4lep.root")
inputBG2 = TFile.Open("../Datasets/mc_345337.ZH125J_llWW2lep.4lep.root")

signal = inputSignal.Get("mini")
bg1 = inputBG1.Get("mini")
bg2 = inputBG2.Get("mini")

# book the TMVA methods
factory = TMVA.Factory("TMVAClassification", outputFile, "!V:!DrawProgressBar:Color:AnalysisType=Classification") 
dataloader = TMVA.DataLoader("dataloader_"+outNameLabel)

# define variables
# TODO: add more variables here
dataloader.AddVariable("lep_pt[0]",'F')
dataloader.AddVariable("lep_pt[0]+lep_pt[1]+lep_pt[2]+lep_pt[3]",'F')
dataloader.AddVariable("met_et",'F')
dataloader.AddVariable("jet_n",'I')

# TODO: further pre-select/clean-up data
selectionSig = "(trigE>0 || trigM > 0) && lep_n>=4 && lep_pt[0]<200e3 && (lep_pt[0]+lep_pt[1]+lep_pt[2]+lep_pt[3])<500e3 && met_et<200e3" 
selectionBG = selectionSig

# add trees and weights
dataloader.AddSignalTree(signal)
dataloader.AddBackgroundTree(bg1)
dataloader.AddBackgroundTree(bg2)

# event weights are added here - no problem for the BDT be careful with others
dataloader.SetSignalWeightExpression("mcWeight*scaleFactor_LepTRIGGER")
dataloader.SetBackgroundWeightExpression("mcWeight*scaleFactor_LepTRIGGER")

# TODO: change number of training and testing events here
dataloader.PrepareTrainingAndTestTree(TCut(selectionSig), TCut(selectionBG),
                                        "nTrain_Signal=5000:nTrain_Background=5000:nTest_Signal=5000:nTest_Background=5000:SplitMode=Random:NormMode=NumEvents:!V")


# In[ ]:


# TODO: change training parameter settings here
factory.BookMethod(dataloader, TMVA.Types.kBDT, "BDT","!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5:PruneMethod=NoPruning")


# In[ ]:


# Training a Neural Network using the Keras Backend

# Define NN model
model = Sequential()
model.add(Dense(64, activation='relu', kernel_regularizer=l2(1e-5), input_dim=4))
model.add(Dense(64, activation='relu', kernel_regularizer=l2(1e-5)))
model.add(Dense(2, activation='softmax'))

# Set loss and optimizer
model.compile(loss='categorical_crossentropy',
              optimizer=SGD(learning_rate=0.01), metrics=['accuracy', ])

# Store model to file
model.save('model_tmva_class_keras.h5')
model.summary()

factory.BookMethod(dataloader, TMVA.Types.kPyKeras, 'PyKeras',
                   'H:!V:VarTransform=D,G:FilenameModel=model_tmva_class_keras.h5:NumEpochs=20:BatchSize=32')


# In[ ]:


factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()


# In[ ]:


# We can now compare with our first BDT
cROC = factory.GetROCCurve(dataloader)
cROC.Draw()


# ## Exploring the methods using the TMVAGui
# 
# TMVA comes with the TMVAGui, which helps understand how the algorithms behave. To run it, simply run
# ```
# root -l -e 'TMVA::TMVAGui("tmvaTraining.root")'
# ```
# from your terminal.
# 
